Markdown
========

Required Debian packages:

$ sudo apt-get install --no-install-recommends \
  latex-beamer latexmk pandoc texlive-latex-base texlive-pictures

To generate a PDF, run this:

$ make markdown
